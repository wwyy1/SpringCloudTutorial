package com.springms.cloud.fhir;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
@SpringBootApplication
@EnableEurekaClient
public class SampleJpaRestfulServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SampleJpaRestfulServerApplication.class, args);
        System.out.println("【【【【【【 Fhir微服务 】】】】】】已启动.");
    }
}
